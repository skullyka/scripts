#!/usr/bin/env python3

import sys

def mirror_mins(mins):
    mirrored_mins = 60 - mins
    carry = 0
    if mirrored_mins == 60:
        carry = 1
        mirrored_mins = 0
    return (mirrored_mins, carry)

def mirror_hrs(hrs, carry):
    mirrored_hrs = 11 - hrs + carry
    if mirrored_hrs <= 0:
        mirrored_hrs += 12
    return mirrored_hrs


def mirror(hr,mins):
    m_mins,carry = mirror_mins(mins)
    m_hrs = mirror_hrs(hr,carry)
    return (m_hrs,m_mins)

def split_up(time):
    time_list = time.split(":")
    return (int(x) for x in time_list)

if __name__ == "__main__":
    hr,mins = split_up(sys.argv[1])
    print(mirror(hr,mins))
